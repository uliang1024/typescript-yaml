import { currentIndex } from './display';

export function downloadAllFiles(links: HTMLAnchorElement[]): void {
    const downloadAllButton = document.getElementById('download_all_button_id');
    downloadAllButton?.addEventListener('click', () => {
        if (links.length > 0) {
            links.forEach(link => {
                link.click();
            });
        } else {
            console.error('No files to download.');
        }
    });
}

export function downloadFileAtIndex(links: HTMLAnchorElement[]): void {
    const downloadButton = document.getElementById('download_button_id');
    downloadButton?.addEventListener('click', () => {
        if (links.length > 0) {
            links[currentIndex].click();
        } else {
            console.error('No file to download.');
        }
    });
}

export function copyFileContent(links: HTMLAnchorElement[]): void {
    const copyButton = document.getElementById('copy_button_id');
    copyButton?.addEventListener('click', () => {
        if (currentIndex >= 0 && currentIndex < links.length) {
            const link = links[currentIndex];
            fetchFileContent(link.href)
                .then(content => copyContentToClipboard(content))
                .catch(error => console.error('Error fetching or copying file content:', error));
        } else {
            console.error('Invalid index.');
        }
    });
}

async function fetchFileContent(url: string): Promise<string> {
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Network response was not ok');
    }
    return response.text();
}

async function copyContentToClipboard(content: string): Promise<void> {
    try {
        await navigator.clipboard.writeText(content);
        console.log('Copied content to clipboard:', content);
    } catch (error) {
        console.error('Error copying content to clipboard:', error);
    }
}
