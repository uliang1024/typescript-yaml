import * as YAML from 'yaml';
import "bootstrap/dist/css/bootstrap.css";
import { generateTypescriptFromYaml } from './typeUtils';
import { displayHtmlContent } from './display';
import { copyFileContent, downloadAllFiles, downloadFileAtIndex } from './buttonFunctions';
import "@fortawesome/fontawesome-free/css/all.min.css";
import { UploadStatus } from './enums';

// 儲存文件下載連結的陣列，<a> 標籤元素的型態
const links: HTMLAnchorElement[] = [];

// 處理上傳的文件
function handleFiles(files: FileList | null) {
    if (!files || files.length === 0) {
        console.error('未選擇任何文件。');
        return;
    }

    // 遍歷每個文件並處理
    Array.from(files).forEach(processFile);
}

// 處理單個文件
function processFile(file: File) {
    const reader = new FileReader();

    // 檢查檔案副檔名是否為 yaml 或 yml
    const extension = file.name.split('.').pop()?.toLowerCase();
    if (extension !== 'yaml' && extension !== 'yml') {
        console.error(`${file.name} 不是 YAML 文件，無法轉換。`);
        return UploadStatus.INVALIDFILETYPE;
    }

    const MAX_FILE_SIZE = 5 * 1024 * 1024; // 5MB
    // 判断文件大小是否超出限制
    if (file.size > MAX_FILE_SIZE) {
        console.error('文件大小不得超出5MB！');
        return UploadStatus.FILETOOLARGE;
    }

    reader.onload = function (event) {
        // 讀取文件內容並解析成 YAML 格式
        const yamlContent = event.target?.result as string;

        try {
            // 使用 YAML 函式庫的 parse 方法，將格式化的 yamlContent 字串參數解析為對應的 JavaScript 對象
            const parsedData = YAML.parse(yamlContent);

            // 生成 TypeScript 代碼
            const interfaces: string = generateTypescriptFromYaml(parsedData);

            // 修改文件名的副檔名為 .ts
            const tsFileName = `${file.name.replace(/\.[^/.]+$/, '')}.ts`;

            // 創建 Blob 對象
            const blob = new Blob([interfaces], { type: 'text/plain' });

            // 創建下載連結元素
            const link = document.createElement('a');
            link.href = URL.createObjectURL(blob);
            link.download = tsFileName;

            // 將連結添加到陣列中
            links.push(link);
            // 顯示文件內容在 HTML 中
            displayHtmlContent(interfaces, file.name, links.length - 1);

            return UploadStatus.SUCCESS;
        } catch (error) {
            console.error(`解析 YAML 文件 ${file.name} 時發生錯誤：`, error);
            return UploadStatus.FAILURE;
        }
    };

    // 讀取文件為文本格式
    reader.readAsText(file);
}

// 當 DOM 元素加載完成後執行以下操作
document.addEventListener('DOMContentLoaded', () => {
    // 設置按鈕的點擊事件
    downloadAllFiles(links);
    downloadFileAtIndex(links);
    copyFileContent(links);

    const fileInput = document.querySelector('#uploadForm input[type="file"]') as HTMLInputElement;
    const paragraph = document.querySelector("#uploadForm p");

    fileInput.addEventListener("change", function () {
        if (fileInput.files.length === 0) {
            paragraph.textContent = "沒有選擇文件";
        } else {
            console.log(fileInput.files.length);
            paragraph.textContent = fileInput.files.length + " 個文件已選擇";
        }
    });

    const uploadButton = document.querySelector('#uploadButton');

    // 監聽上傳按鈕的點擊事件
    uploadButton.addEventListener('click', () => {
        // 處理上傳的文件
        handleFiles(fileInput.files);

        // 清空files
        fileInput.value = '';
        fileInput.files = null;
        paragraph.textContent = '單一文件拖曳到此處或多項文件按一下該區域。'
    });
});