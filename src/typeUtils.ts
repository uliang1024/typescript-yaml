import pluralize from 'pluralize';

interface ValueTypeMap {
    [key: string]: string;
}

export function getTypeOfValue(parsedData: object, originalName: string): ValueTypeMap {
    const properties: ValueTypeMap = {};

    const user: object = parsedData[originalName]?.[0];

    const keys: string[] = Object.keys(user);

    for (const key of keys) {
        const value = user[key];
        let type: string = "null";

        if (Array.isArray(value)) {
            // 判斷值是否為數組，且數組中的所有元素是否都屬於同一類型
            const elementType = value.length > 0 ? typeof value[0] : null;
            const isHomogeneousArray = elementType !== null && value.every(item => typeof item === elementType);

            if (isHomogeneousArray) {
                // 如果值是相同類型的數組，則設定為該類型的數組
                type = `${elementType}[]`;
            } else {
                // 否則設定為數組
                type = 'Array<any>';
            }
        } else if (typeof value === 'object' && value !== null) {
            // 如果值是對象，則設定為對象
            type = 'object';
        } else {
            // 否則設定為值的類型
            type = typeof value;
        }

        properties[key] = type;
    }

    return properties;
}

// 英文單字第一個字大寫，並轉單數
export function generateInterfaceName(source: string): string {
    return source.charAt(0).toUpperCase() + pluralize.singular(source).slice(1);
}

export function generateTypescriptFromYaml(parsedData: object): string {
    console.log(parsedData);
    
    const originalName: string = Object.keys(parsedData)[0];
    const modifiedName: string = generateInterfaceName(originalName);
    const valueType: ValueTypeMap = getTypeOfValue(parsedData, originalName);

    let interfaces: string = `interface ${modifiedName} {\n`;

    for (const [key, value] of Object.entries(valueType)) {
        interfaces += `    ${key}: ${value};\n`;
    }
    interfaces += `}\n\n`;

    interfaces += `interface ${modifiedName}List {\n`;
    interfaces += `    ${originalName}: ${modifiedName}[];\n`;
    interfaces += `}\n\n`;

    interfaces += `export const ${originalName}: ${modifiedName}List = ${JSON.stringify(parsedData, null, 4)};\n`;

    return interfaces;
}
