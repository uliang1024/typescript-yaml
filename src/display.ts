import Prism from 'prismjs';
import 'prismjs/components/prism-typescript';
import 'prismjs/themes/prism-okaidia.css'
import 'prismjs/plugins/line-numbers/prism-line-numbers.js'
import 'prismjs/plugins/line-numbers/prism-line-numbers.css'

export let currentIndex: number = 0;

export function displayHtmlContent(interfaces: string, fileName: string, index: number): void {
    const button: HTMLButtonElement = createButton(fileName, interfaces, index);
    const rootElement: HTMLElement = document.getElementById("app");
    rootElement.appendChild(button);
}

function createButton(fileName: string, interfaces: string, index: number): HTMLButtonElement {
    const button: HTMLButtonElement = document.createElement("button");
    button.classList.add("btn", "btn-primary");
    button.type = "button";

    const fileNameWithoutExtension: string = fileName.replace(/\.[^.]+$/, '');

    button.textContent = `${fileName} => ${fileNameWithoutExtension}.ts`;

    button.addEventListener("click", () => {
        const preElement: Element = document.querySelector("#show-convert");
        preElement.innerHTML = `<code class="language-typescript">${interfaces}</code>`;
        Prism.highlightElement(preElement);
        currentIndex = index;
    });

    return button;
}