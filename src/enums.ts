export enum UploadStatus {
    SUCCESS = 1,
    FAILURE = 2,
    INVALIDFILETYPE = 3,  // 上傳的檔案類型不被支援
    FILETOOLARGE = 4,     // 上傳的檔案大小超出了限制
}

export function processStatus(status: UploadStatus) {

    switch (status) {
        case UploadStatus.SUCCESS:
            console.log('檔案處理成功！');
            break;
        case UploadStatus.FAILURE:
            console.error('檔案處理失敗！');
            break;
        case UploadStatus.FILETOOLARGE:
            console.error('檔案大小超出限制！');
            break;
        case UploadStatus.INVALIDFILETYPE:
            console.error('檔案類型無效！');
            break;
        default:
            console.error('未知錯誤！');
            break;
    }
}
