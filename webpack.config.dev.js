export default {
  mode: "development",
  entry: "./src/index.ts",
  output: {
    filename: "bundle.js",
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  module: {
    rules: [
      {
        test: /\.ts/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  devtool: "inline-source-map", // 添加调试源映射
  devServer: {
    static: "./assets",
    port: 8000,
  },
};
