import TerserPlugin from "terser-webpack-plugin";

const config = {
  mode: "production",
  entry: "./src/index.ts",
  output: {
    filename: "bundle.js",
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  devtool: "source-map", // 通常在生产模式下使用更轻量级的source map
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  },
};

export default config;
